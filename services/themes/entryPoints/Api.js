const ThemesService = require("../ThemesService");

const ApiMapper = require("../mappers/ApiMapper");
const CoreApi = require("../../core/entryPoints/Api");

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/themes": {
                    act: this.__getMethod('getAll')
                },
                "/themes/:id": {
                    act: this.__getMethod('getItem')
                },
            },
            "post": {
                "/themes": {
                    act: this.__getMethod('createItem')
                }
            },

            "patch": {
                "/themes/:id": {
                    act: this.__getMethod('updateItem')
                }
            },
            "delete": {
                "/themes/:id": {
                    act: this.__getMethod('deleteItem')
                }
            },
        };
    }

    /**
     * Returns todolist collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getAll(req, res)
    {

        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let list = await new ThemesService().getThemesFilteredCollection(requestObject);
        if(!list)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(list, new ApiMapper())
        );
    }

    /**
     * Returns todolist item or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getItem(req, res)
    {
        let item = await new ThemesService().addUserDataFilter(this.__getUserFilter(req)).getItem(req.params.id);

        if(!item)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(item, new ApiMapper())
        );
    }

    /**
     * Create new item and returns created item data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createItem(req, res)
    {
        let mappedObject = new ApiMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let item = await new ThemesService().create(mappedObject);

        res.status(200).send(
            this.mapEntityToClient(item, new ApiMapper() )
        );
    }

    /**
     * Update existing item in list and returns updated item data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.uid  User uid.
     */
    async updateItem(req, res)
    {
        let mappedObject = new ApiMapper().toServerProperty(req.body);

        let item = await new ThemesService().addUserDataFilter(this.__getUserFilter(req)).updateItem(req.params.id, mappedObject);
        if(!item)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(item, new ApiMapper())
        );
    }

    /**
     * Delete existing item and returns 201 status no content or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.uid  User uid.
     */
    async deleteItem(req, res)
    {
        let result = await new ThemesService().addUserDataFilter(this.__getUserFilter(req)).deleteItem(req.params.id);
        if(!result)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(201).send();
    }
}

module.exports = Api;
