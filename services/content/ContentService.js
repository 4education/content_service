
const Category = require("./models/Category");
const CourseModel = require("./models/Course");
const ModuleModel = require("./models/Module");
const LessonModel = require("./models/Lesson");
const AssignedUsersModel = require("./models/AssignedUsers");

const InternalError = require("./exceptions/InternalError");
const serviceContainer = require("../../libs/ServiceContainer");
const events = require("./config/Events");


class ContentService
{
    async getCategory(id)
    {
        const gr = await this.__getCategoryModel().getById(id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    getCategoriesFilteredCollection(requestObject)
    {
        return this.__getCategoryModel().executeQuery(requestObject);
    }

    async createCategory(cat)
    {
        const newCat = await this.__getCategoryModel().create(cat);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.CATEGORY_CREATED,
            data: createdData
        });

        return createdData;
    }

    async updateCategoryById(cat, id)
    {
        const newCat = await this.__getCategoryModel().updateItemById(cat, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.CATEGORY_UPDATED,
            data: createdData
        });

        return createdData;
    }

    removeCategoryById(id)
    {
        const res = this.__getCategoryModel().removeById(id);
        if(!res)
        {
            throw new InternalError("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.CATEGORY_DELETED,
            data: {_id: id}
        });
        return res;
    }

    async getModule(id)
    {
        const gr = await this.__getModuleModel().getById(id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    getModulesFilteredCollection(requestObject)
    {
        return this.__getModuleModel().executeQuery(requestObject);
    }

    async createModule(cat)
    {
        const newCat = await this.__getModuleModel().create(cat);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.MODULE_CREATED,
            data: createdData
        });

        return createdData;
    }

    async updateModule(cat, id)
    {
        const newCat = await this.__getModuleModel().updateItemById(cat, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.MODULE_UPDATED,
            data: createdData
        });

        return createdData;
    }

    removeModuleById(id)
    {
        const res = this.__getModuleModel().removeById(id);
        if(!res)
        {
            throw new InternalError("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.MODULE_DELETED,
            data: {_id: id}
        });
        return res;
    }

    async getCourse(id)
    {
        const gr = await this.__getCourseModel().getById(id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    getCoursesFilteredCollection(requestObject)
    {
        return this.__getCourseModel().executeQuery(requestObject);
    }

    async createCourse(cat)
    {
        const newCat = await this.__getCourseModel().create(cat);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.COURSE_CREATED,
            data: createdData
        });

        return createdData;
    }

    async updateCourse(cat, id)
    {
        const newCat = await this.__getCourseModel().updateItemById(cat, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.COURSE_UPDATED,
            data: createdData
        });

        return createdData;
    }

    removeCourseById(id)
    {
        const res = this.__getCourseModel().removeById(id);
        if(!res)
        {
            throw new InternalError("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.COURSE_DELETED,
            data: {_id: id}
        });
        return res;
    }

    async getLesson(id)
    {
        const gr = await this.__getLessonModel().getById(id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    getLessonsCollection(requestObject)
    {
        return this.__getLessonModel().executeQuery(requestObject);
    }

    async createLesson(cat)
    {
        const newCat = await this.__getLessonModel().create(cat);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.COURSE_CREATED,
            data: createdData
        });

        return createdData;
    }

    async updateLesson(cat, id)
    {
        const newCat = await this.__getLessonModel().updateItemById(cat, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.COURSE_UPDATED,
            data: createdData
        });

        return createdData;
    }

    removeLessonById(id)
    {
        const res = this.__getLessonModel().removeById(id);
        if(!res)
        {
            throw new InternalError("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.COURSE_DELETED,
            data: {_id: id}
        });
        return res;
    }

    addUserDataFilter(userData)
    {
        this.userTofilter = userData;
        return this;
    }

    async assignUserToCourse(uid, id)
    {
        const newCat = await this.__getAssignedUsersModel().addUser(uid, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.USER_ASSIGNED,
            data: createdData
        });

        return createdData;
    }

    async assignUserGroupToCourse(guid, id)
    {
        const newCat = await this.__getAssignedUsersModel().addUserGroup(guid, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.GROUP_ASSIGNED,
            data: createdData
        });

        return createdData;
    }

    async removeUserFromCourse(uid, id)
    {
        const newCat = await this.__getAssignedUsersModel().removeUser(uid, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.USER_UNASSIGNED,
            data: createdData
        });

        return createdData;
    }

    async removeUserGroupFromCourse(guid, id)
    {
        const newCat = await this.__getAssignedUsersModel().removeUserGroup(guid, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.GROUP_UNASSIGNED,
            data: createdData
        });

        return createdData;
    }

    async getAssignedCollection(query)
    {
        return this.__getAssignedUsersModel().executeQuery(query);
    }

    async getCourseAssign(courseId)
    {
        const gr = await this.__getAssignedUsersModel().getById(courseId);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    async createSection(materialSec, id)
    {
        const newCat = await this.__getLessonModel().createSection(materialSec, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};
        return createdData;
    }

    async updateSection(materialSec, sid, id)
    {
        const newCat = await this.__getLessonModel().updateSection(materialSec, sid, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};
        return createdData;
    }

    async removeSection(sId, id)
    {
        const newCat = await this.__getLessonModel().removeSection(sId, id);
        let createdData = newCat && newCat.toObject ? newCat.toObject() : {};
        return createdData;
    }

    __getCategoryModel()
    {
        return new Category().addAdditionalConditions(this.userTofilter);
    }

    __getCourseModel()
    {
        return new CourseModel().addAdditionalConditions(this.userTofilter);
    }

    __getModuleModel()
    {
        return new ModuleModel().addAdditionalConditions(this.userTofilter);
    }

    __getLessonModel()
    {
        return new LessonModel().addAdditionalConditions(this.userTofilter);
    }

    __getAssignedUsersModel()
    {
        return new AssignedUsersModel().addAdditionalConditions(this.userTofilter);
    }


    __getEventManager()
    {
        return  serviceContainer.getService('internal', 'eventService');
    }
}

module.exports = ContentService;
