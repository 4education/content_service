# example of backend

# Require

- docker

# Install

```
sudo apt-get install -y docker-engine
```
if you have a trouble please follow link

```
https://www.digitalocean.com/community/tutorials/docker-ubuntu-16-04-ru
```
#
*All operation should be run in docker project dir*

# Dockerfile

this files uses for build container only. If you need specify
libraris or tools in some container add it to Dockerfile
and run rebuild

```
docker-compose build
```

# env

all needed variables should be changed in .env file


# Launch

```
docker-compose up
```


# Run devscripts in containers

```
docker exec -it docker_configuration_1 /bin/sh

node scripts/dev_run_script.js install_fixture
```
