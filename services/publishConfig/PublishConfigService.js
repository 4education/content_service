//this wrapper should incoupsulate all business logic
const PublishConfModel = require("./models/PublishConfModel");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
const InternalErrorException = require("./exceptions/InternalError");
const serviceContainer = require("../../libs/ServiceContainer");
const events = require("./config/Events");

class PublishConfigService
{
    getAll()
    {
        return this.__getModel().getAll();
    }

    getPublishingConfigFilteredCollection (requestObject)
    {
        return this.__getModel().executeQuery(requestObject);
    }

    async create(publishConf)
    {
        const newpConf = await this.__getModel().create(publishConf);

        let createdData = newpConf && newpConf.toObject ? newpConf.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.PUBLISH_CONF_CREATED,
            data: createdData
        });
        return createdData;
    }

    async getItem(id)
    {
        const newpConf = await this.__getModel().getById(id);

        return newpConf && newpConf.toObject ? newpConf.toObject() : {};
    }

    async updateItem(id, todoItem)
    {
        let item = await this.__getModel().getById(id);
        if(!item)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().update(id, todoItem);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        return updatedItem.toObject() ;
    }

    async deleteItem(id)
    {
        let item = await this.__getModel().getById(id);
        if(!item)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().remove(id);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        this.__getEventManager().dispatch({
            type: events.PUBLISH_CONF_DELETED,
            data: {_id: id}
        });

        return true ;
    }

    __getModel()
    {
        return new PublishConfModel().addAdditionalConditions(this.userTofilter);
    }

    addUserDataFilter(userData)
    {
        this.userTofilter = userData;
        return this;
    }

    __getEventManager()
    {
        return  serviceContainer.getService('internal', 'eventService');
    }
}

module.exports = PublishConfigService;
