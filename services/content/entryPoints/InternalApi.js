const testService = require("../TestService");

class InternalApi
{
    /**
     * @param {String} id Unique test id
     *
     * @return {Object} testData Method returns User object data.
     * @return {String} User.email user email.
     * @return {String} User.id user id.
     * @return {Bool} User.isActive  Flag that shows is user active or not.
     * @return {String} User.role  Enum parameter which shows what is user role.
     */
    getTestById(id)
    {
        return new testService().getTest(id);
    }

    getTestQuestions(email)
    {
        //return userService.getUser(email);
    }

    getTestByIdWithQuestions(id)
    {
        return new Promise((resolve, reject) => {
            testService.getTest(id).then( testData => {
                if(!testData) return reject(null);

                testService.getTestQuestions(id).then(questions => {
                    if(!questions) return reject(null);

                    testData.questions = questions;
                    resolve(testData);
                });
            }).catch(e => {reject(e)});
        });
    }
}

module.exports = InternalApi;