const contentService = require("../ContentService");
const serviceContainer = require("../../../libs/ServiceContainer");
const ApiCategoryMapper = require("../mappers/ApiCategoryMapper");
const ApiCoursesMapper = require("../mappers/ApiCoursesMapper");
const ApiModulesMapper = require("../mappers/ApiModulesMapper");
const ApiLessonMapper = require("../mappers/ApiLessonMapper");
const ApiAssignedUsersMapper = require("../mappers/ApiAssignedUsersMapper");
const RequestQuery = require("../../../libs/RequestQuery");
const ApiMaterialsMapper = require("../mappers/ApiMaterialsMapper");

const mongoose = require('../../../libs/mongoose').mongoose;

const CoreApi = require("../../core/entryPoints/Api");
const { protocol, launcher_origin } = require("../../../config/server");
const path = require('path');

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/categories": {
                    act: this.__getMethod('getCategories')
                },
                "/categories/:id": {
                    act: this.__getMethod('getCategory')
                },
                "/categories/:id/courses": {
                    act: this.__getMethod('getCategoryCourses')
                },
                "/courses": {
                    act: this.__getMethod('getCourses')
                },
                "/courses/:id": {
                    act: this.__getMethod('getCourse')
                },
                "/courses/:cid/modules": {
                    act: this.__getMethod('getCategoryCoursesModules')
                },
                "/modules": {
                    act: this.__getMethod('getModules')
                },
                "/modules/:id": {
                    act: this.__getMethod('getModule')
                },
                "/courses/:cid/modules/:mid/lessons": {
                    act: this.__getMethod('getCategoryCoursesModulesLessons')
                },
                "/lessons": {
                    act: this.__getMethod('getLessons')
                },
                "/lessons/:id": {
                    act: this.__getMethod('getLesson')
                },

                // CUSTOM ACTIONS
                "/my_courses": {
                    act: this.__getMethod('getMyCourses')
                },

                "/courses/:cid/assigned_users": {
                    act: this.__getMethod('getAssignedUsers')
                },
                "/courses/:cid/assigned_usergroups": {
                    act: this.__getMethod('getAssignedUserGroups')
                },
                "/courses/:cid/not_assigned_users": {
                    act: this.__getMethod('getUnAssignedUsers')
                },
                "/courses/:cid/not_assigned_usergroups": {
                    act: this.__getMethod('getUnAssignedUserGroups')
                },
            },
            "post": {
                "/categories": {
                    act: this.__getMethod('createCategory')
                },
                "/courses": {
                    act: this.__getMethod('createCourse')
                },
                "/modules": {
                    act: this.__getMethod('createModule')
                },
                "/lessons": {
                    act: this.__getMethod('createLesson')
                },
                "/lessons/:lid/materials": {
                    act: this.__getMethod('createLessonSection')
                },
                // CUSTOM ACTIONS
                "/course/add_user": {
                    act: this.__getMethod('assignCourseToUser')
                },
                "/course/remove_user": {
                    act: this.__getMethod('removeCourseFromUser')
                },
                "/course/add_usergroup": {
                    act: this.__getMethod('assignCourseToUserGroup')
                },
                "/course/remove_usergroup": {
                    act: this.__getMethod('removeCourseFromUserGroup')
                },

                //INTERNAL
                "/internal/get_lesson": {
                    act: this.__getMethod('getLessonData')
                },
                "/internal/get_courses": {
                    act: this.__getMethod('getCoursesData')
                },
                "/internal/get_modules": {
                    act: this.__getMethod('getModulesData')
                },
            },
            "delete":{
                "/categories/:id": {
                    act: this.__getMethod('removeCategory')
                },
                "/courses/:id": {
                    act: this.__getMethod('removeCourse')
                },
                "/modules/:id": {
                    act: this.__getMethod('removeModule')
                },
                "/lessons/:id": {
                    act: this.__getMethod('removeLesson'),
                },
                "/lessons/:lid/materials/:sid": {
                    act: this.__getMethod('removeLessonSection')
                },
            },
            "patch":{
                "/categories/:id": {
                    act: this.__getMethod('updateCategory')
                },
                "/courses/:id": {
                    act: this.__getMethod('updateCourse')
                },
                "/modules/:id": {
                    act: this.__getMethod('updateModule')
                },
                "/lessons/:id": {
                    act: this.__getMethod('updateLesson')
                },
                "/lessons/:lid/materials/:sid": {
                    act: this.__getMethod('updateLessonSection')
                },
            },
        };
    }
    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getCategories(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCategoriesFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiCategoryMapper())
        );
    }

    /**
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async getCategory(req, res)
    {
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCategory(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(gr, new ApiCategoryMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createCategory(req, res)
    {
        let mappedObject = new ApiCategoryMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).createCategory(mappedObject);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiCategoryMapper())
        );
    }

    /**
     * Returns updated test group or 404 if group not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateCategory(req, res)
    {
        let gr  = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCategory(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Category with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        let mappedObject = new ApiCategoryMapper().toServerProperty(req.body);
        delete mappedObject.cid;

        let cat = await new contentService().updateCategoryById(mappedObject, req.params.id);
        if(!cat)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(cat, new ApiCategoryMapper())
        );
    }

    /**
     * Remove test group return 201 or 404 if category not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async removeCategory(req, res)
    {
        let cat = await new contentService().addUserDataFilter(this.__getUserFilter(req)).removeCategoryById(req.params.id);
        if(!cat)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(201).send();
    }
    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getCourses(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCoursesFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiCoursesMapper())
        );
    }

    /**
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async getCourse(req, res)
    {
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCourse(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(gr, new ApiCoursesMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createCourse(req, res)
    {
        let mappedObject = new ApiCoursesMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).createCourse(mappedObject);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiCoursesMapper())
        );
    }

    /**
     * Returns updated test group or 404 if group not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateCourse(req, res)
    {
        let gr  = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCourse(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Category with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        let mappedObject = new ApiCoursesMapper().toServerProperty(req.body);
        delete mappedObject.cid;

        let cat = await new contentService().updateCourse(mappedObject, req.params.id);
        if(!cat)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(cat, new ApiCoursesMapper())
        );
    }

    /**
     * Remove test group return 201 or 404 if category not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async removeCourse(req, res)
    {
        let cat = await new contentService().addUserDataFilter(this.__getUserFilter(req)).removeCourseById(req.params.id);
        if(!cat)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(201).send();
    }
    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getModules(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getModulesFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiModulesMapper())
        );
    }

    /**
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async getModule(req, res)
    {
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getModule(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(gr, new ApiModulesMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createModule(req, res)
    {
        let mappedObject = new ApiModulesMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).createModule(mappedObject);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiModulesMapper())
        );
    }

    /**
     * Returns updated test group or 404 if group not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateModule(req, res)
    {
        let gr  = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getModule(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Module with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        let mappedObject = new ApiModulesMapper().toServerProperty(req.body);
        delete mappedObject.cid;

        let cat = await new contentService().updateModule(mappedObject, req.params.id);
        if(!cat)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(cat, new ApiModulesMapper())
        );
    }

    /**
     * Remove test group return 201 or 404 if category not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async removeModule(req, res)
    {
        let cat = await new contentService().addUserDataFilter(this.__getUserFilter(req)).removeModuleById(req.params.id);
        if(!cat)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(201).send();
    }


    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getLessons(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getLessonsCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiLessonMapper())
        );
    }

    /**
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async getLesson(req, res)
    {
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getLesson(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createLesson(req, res)
    {
        let mappedObject = new ApiLessonMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).createLesson(mappedObject);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createLessonSection(req, res)
    {
        let mappedObject = ApiMaterialsMapper.toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).createSection(mappedObject, req.params.lid);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateLessonSection(req, res)
    {
        let mappedObject = ApiMaterialsMapper.toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).updateSection(mappedObject, req.params.sid, req.params.lid);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async removeLessonSection(req, res)
    {
        let gr = await new contentService().addUserDataFilter(this.__getUserFilter(req)).removeSection(req.params.sid, req.params.lid);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    /**
     * Returns updated test group or 404 if group not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateLesson(req, res)
    {
        let gr  = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getLesson(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Module with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        let mappedObject = new ApiLessonMapper().toServerProperty(req.body);
        delete mappedObject.cid;

        let cat = await new contentService().updateLesson(mappedObject, req.params.id);
        if(!cat)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(cat, new ApiLessonMapper())
        );
    }

    /**
     * Remove test group return 201 or 404 if category not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async removeLesson(req, res)
    {
        let cat = await new contentService().addUserDataFilter(this.__getUserFilter(req)).removeLessonById(req.params.id);
        if(!cat)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(201).send();
    }

    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getCategoryCourses(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        requestObject.addWhereConditionsForCustomObject('catId',mongoose.Types.ObjectId(req.params.id));
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getCoursesFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiCoursesMapper())
        );
    }

    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getCategoryCoursesModules(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        requestObject.addWhereConditionsForCustomObject('courseId',mongoose.Types.ObjectId(req.params.cid));
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getModulesFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiModulesMapper())
        );
    }

    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getCategoryCoursesModulesLessons(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        requestObject.addWhereConditionsForCustomObject('moduleId',mongoose.Types.ObjectId(req.params.mid));
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new contentService().addUserDataFilter(this.__getUserFilter(req)).getLessonsCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiLessonMapper())
        );
    }

    async getMyCourses(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        const userGroups = await serviceContainer.getService('internal', 'usersService').getUserGroups(req.user.uid);
        const ids = userGroups.items.map((item) => {
            return mongoose.Types.ObjectId(item.gid)
        });
        requestObject.addWhereConditionsForCustomObject('users',mongoose.Types.ObjectId(req.user.uid));
        //requestObject.addWhereConditions('groups',{in: ids});
        let courses = await new contentService().getAssignedCollection(requestObject);
        const coursesIds = courses.items.map((item) => {
            return mongoose.Types.ObjectId(item.course[0]._id);
        });
        let newRequest = new RequestQuery();
        newRequest.addWhereConditions('_id',{in:coursesIds});
        let coursesCollection = await new contentService().getCoursesFilteredCollection(newRequest);
        if(!coursesCollection)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(coursesCollection, new ApiCoursesMapper())
        );
    }


    async getUnAssignedUsers(req, res)
    {
        let assign = await new contentService().getCourseAssign(req.params.cid);
        const {users}  = assign || {};
        const usersColl = await serviceContainer.getService('internal', 'usersService')
            .getUsersNotInList(users || [], req.user.cid, this.getPaginationParams(req));
        if(usersColl)
        {
            return res.status(200).send(
                usersColl
            );
        }

        res.status(200).send(
            {"items":[],"totalCount":0}
        );
    }

    async getUnAssignedUserGroups(req, res)
    {
        let assign = await new contentService().getCourseAssign(req.params.cid);
        const {userGroups}  = assign || {};
        const userGroupsColl = await serviceContainer.getService('internal', 'usersService')
            .getGroupsNotInList(userGroups || [], req.user.cid, this.getPaginationParams(req));
        if(userGroupsColl)
        {
            return res.status(200).send(
                userGroupsColl
            );
        }

        res.status(200).send(
            {"items":[],"totalCount":0}
        );
    }

    async getAssignedUsers(req, res)
    {
        let assign = await new contentService().getCourseAssign(req.params.cid);
        const userGroups = await serviceContainer.getService('internal', 'usersService')
            .getUsersInList(assign.users, req.user.cid, this.getPaginationParams(req));
        if(userGroups)
        {
            return res.status(200).send(
                userGroups
            );
        }

        res.status(200).send(
            {"items":[],"totalCount":0}
        );
    }

    async getAssignedUserGroups(req, res)
    {
        let assign = await new contentService().getCourseAssign(req.params.cid);
        const userGroups = await serviceContainer.getService('internal', 'usersService')
            .getGroupsInList(assign.usersGroups, req.user.cid, this.getPaginationParams(req));
        if(userGroups)
        {
            return res.status(200).send(
                userGroups
            );
        }

        res.status(200).send(
            {"items":[],"totalCount":0}
        );
    }

    async assignCourseToUser(req, res)
    {
        let mappedObject = new ApiAssignedUsersMapper().toServerProperty(req.body);
        let gr = await new contentService().assignUserToCourse(mappedObject.uid, mappedObject.courseId);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiAssignedUsersMapper())
        );
    }

    async assignCourseToUserGroup(req, res)
    {
        let mappedObject = new ApiAssignedUsersMapper().toServerProperty(req.body);
        let gr = await new contentService().assignUserGroupToCourse(mappedObject.guid, mappedObject.courseId);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiAssignedUsersMapper())
        );
    }

    async removeCourseFromUser(req, res)
    {
        let mappedObject = new ApiAssignedUsersMapper().toServerProperty(req.body);
        let gr = await new contentService().removeUserFromCourse(mappedObject.uid, mappedObject.courseId);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiAssignedUsersMapper())
        );
    }

    async removeCourseFromUserGroup(req, res)
    {
        let mappedObject = new ApiAssignedUsersMapper().toServerProperty(req.body);
        let gr = await new contentService().removeUserGroupFromCourse(mappedObject.guid, mappedObject.courseId);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiAssignedUsersMapper())
        );
    }

    //INTERNAL
    async getLessonData(req, res)
    {
        let gr = await new contentService().getLesson(req.body.lid);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }
        item.statData = {materialSecCount: gr.materialItems.length}
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiLessonMapper())
        );
    }

    async getCoursesData(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        const userGroups = await serviceContainer.getService('internal', 'usersService').getUserGroups(req.body.uid);
        const ids = userGroups.items.map((item) => {
            return mongoose.Types.ObjectId(item.gid)
        });
        requestObject.addWhereConditionsForCustomObject('users',mongoose.Types.ObjectId(req.body.uid));
        requestObject.addWhereConditions('groups',{in: ids});
        let courses = await new contentService().getAssignedCollection(requestObject);
        if(!courses)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        let newRequest = new RequestQuery();
        const coursesIds = courses.items.map((item) => {
            return mongoose.Types.ObjectId(item.course._id);
        });
        newRequest.addWhereConditions('courseId',{in:coursesIds});
        let lessons = await new contentService().getLessonsCollection(newRequest);
        let progressCounts = {};
        lessons.items.map((item) => {
            if(!progressCounts[item.courseId])
            {
                progressCounts[item.courseId] = item.materialItems.length;
            }
            else{
                progressCounts[item.courseId] += item.materialItems.length
            }
            return item;
        });

        newRequest = new RequestQuery();
        newRequest.addWhereConditions('_id',{in:coursesIds});

        let coursesCollection = await new contentService().getCoursesFilteredCollection(newRequest);
        coursesCollection.items = coursesCollection.items.map((item) => {
            if(progressCounts[item._id])
            {
                item.statData = {materialSecCount: progressCounts[item._id]}
            }
            else{
                item.statData = {materialSecCount: 0}
            }
            return item;
        });
        res.status(200).send(
            this.setCollectionResponseData(coursesCollection, new ApiCoursesMapper())
        );
    }

    async getModulesData(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        const userGroups = await serviceContainer.getService('internal', 'usersService').getUserGroups(req.body.uid);
        const ids = userGroups.items.map((item) => {
            return mongoose.Types.ObjectId(item.gid)
        });
        requestObject.addWhereConditionsForCustomObject('users',mongoose.Types.ObjectId(req.body.uid));
        requestObject.addWhereConditions('groups',{in: ids});
        let courses = await new contentService().getAssignedCollection(requestObject);
        if(!courses)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        let newRequest = new RequestQuery();
        const coursesIds = courses.items.map((item) => {
            return mongoose.Types.ObjectId(item.course._id);
        });
        newRequest.addWhereConditions('courseId',{in:coursesIds});
        let lessons = await new contentService().getLessonsCollection(newRequest);
        let progressCounts = {};
        lessons.items.map((item) => {
            if(!progressCounts[item.moduleId])
            {
                progressCounts[item.moduleId] = item.materialItems.length;
            }
            else{
                progressCounts[item.moduleId] += item.materialItems.length
            }
            return item;
        });

        newRequest = new RequestQuery();
        newRequest.addWhereConditions('_id',{in:coursesIds});

        let modulesCollection = await new contentService().getModulesFilteredCollection(newRequest);
        modulesCollection.items = modulesCollection.items.map((item) => {
            if(progressCounts[item._id])
            {
                item.statData = {materialSecCount: progressCounts[item._id]}
            }
            else{
                item.statData = {materialSecCount: 0}
            }
            return item;
        });
        res.status(200).send(
            this.setCollectionResponseData(modulesCollection, new ApiModulesMapper())
        );
    }
}

module.exports = Api;
