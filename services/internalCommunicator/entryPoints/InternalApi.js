const communicatorService = require("../CommunicatorService");

class InternalApi
{
    async createTestLinkData(testId)
    {
        let tokenData = await new communicatorService().createToken(testId);

        return tokenData;
    }
}

module.exports = InternalApi;