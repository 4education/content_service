
const GroupModel = require("./models/Group");
const TestModel = require("./models/Test");
const TestQuestionModel = require("./models/TestQuestions");

const ValidationError = require("./exceptions/ValidationError");
const EntityDuplicateKeyException = require("./exceptions/EntityDuplicateKeyException");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
const InternalError = require("./exceptions/InternalError");
const serviceContainer = require("../../libs/ServiceContainer");
const events = require("./config/Events");


class TestService
{
    async getGroup(id)
    {
        const gr = await this.__getGroupModel().getById(id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    getGroupsFilteredCollection(requestObject)
    {
        return this.__getGroupModel().executeQuery(requestObject);
    }

    getTestGroupsFilteredCollection(requestObject, testId)
    {
        return this.__getGroupModel().getTestGroupsFilteredCollection(requestObject, testId);
    }

    async createGroup(group)
    {

        if(group.pid)
        {
            let pbConf = await serviceContainer.getService('internal', 'publishConfig').getById(group.pid);
            if(!pbConf)
            {
                return new Promise((resolve, reject) => {reject(new ValidationError("Publishing Config wuth id *"+group.pid+"* not exists"))})
            }
        }
        group.testsCount = 0;
        if(group.testsList && group.testsList.length > 0)
        {
            let res = this.__getTestModel().isAllTestsExists(group.testsList);
            if(!res)
            {
                return new Promise((resolve, reject) => {reject(new ValidationError("Not at all tests exists"))})
            }
            group.testsCount = group.testsList.length;
        }
        const newGr = await this.__getGroupModel().create(group);
        if( group.testsCount > 0 )
        {
            this.__getTestModel().addGroupToTests(group.testsList, newGr._id);
        }

        let createdData = newGr && newGr.toObject ? newGr.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.GROUP_CREATED,
            data: createdData
        });

        return createdData;
    }

    async updateGroupById(group, id)
    {
        if(group.testsList && group.testsList.length > 0)
        {
            const tModel = new TestModel();
            let res = tModel.isAllTestsExists(group.testsList);
            if(!res)
            {
                return new Promise((resolve, reject) => {reject(new ValidationError("Not at all tests exists"))})
            }
            console.log(group.testsList);
            await tModel.removeGroupFromTests(id);
            await tModel.addGroupToTests(group.testsList, id);
            group.testsCount = group.testsList.length;
        }
        const gr = await this.__getGroupModel().updateItemById(group, id);
        return gr && gr.toObject ? gr.toObject() : null;
    }

    removeById(id)
    {
        new TestModel().removeGroupFromTests(id);
        const res = this.__getGroupModel().removeById(id);
        if(!res)
        {
            throw new InternalError("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.GROUP_DELETED,
            data: {_id: id}
        });
        return res;
    }

    getTest(id)
    {
        return new Promise((resolve, reject) => {
            this.__getTestModel().getTest(id).then( (testData, err) => {
                if(err) return this.__errorProcessing(err, reject);

                resolve(testData.toObject());
            }).catch(err => {
                this.__errorProcessing(err, reject);
            });
        });
    }

    getTestsCollection(requestObject)
    {
        return new TestModel().executeQuery(requestObject);
    }

    async createTest(test)
    {
        const grModel = this.__getGroupModel();
        const isGroupsExists = test.groupsList && test.groupsList.length > 0 ? await grModel.isAllGroupsExists(test.groupsList) : true;

        if(isGroupsExists)
        {
            const result = await new TestModel().create(test);
            if(result)
            {
                let data = result.toObject();

                this.__getEventManager().dispatch({
                    type: events.TEST_CREATED,
                    data: data
                });
                await grModel.addTestToGroups(data.groupsList);
                return data;
            }
        }
    }

    async updateTest(test, id)
    {
        if(!id)
        {
            throw new ValidationError("id is mandatory field");
        }
        const oldTest = new TestModel().getTest(id);
        if(!oldTest)
        {
            return new NotFoundEntityException("Test with id - "+id+" does not exists");
        }
        const grModel = this.__getGroupModel();
        const isGroupsExists = test.groupsList && test.groupsList.length > 0 ? await grModel.isAllGroupsExists(test.groupsList) : true;
        if(isGroupsExists)
        {
            const result = await new TestModel().updateTestById(test, id);
            if(result)
            {
                let data = result.toObject();

                this.__getEventManager().dispatch({
                    type: events.TEST_UPDATED,
                    data: data
                });
                await grModel.removeTestFromGroups(oldTest.groupsList);
                await grModel.addTestToGroups(data.groupsList);
                return data;
            }
        }
    }

    async removeTest(id)
    {
        if(!id)
        {
            throw new ValidationError("id is mandatory field");
        }
        const oldTest = new TestModel().getTest(id);
        if(!oldTest)
        {
            return new NotFoundEntityException("Test with id - "+id+" does not exists");
        }
        await new TestModel().removeTest(id);
        this.__getEventManager().dispatch({
            type: events.TEST_DELETED,
            data: {id: id}
        });
        await this.__getGroupModel().removeTestFromGroups(oldTest.groupsList);
        return true;
    }

    createTestQuestion(testQuestion, id)
    {
        return new Promise((resolve, reject) => {
            new TestModel().createQuestion(testQuestion, id).then( (testQuestData, err) => {
                if(err) return this.__errorProcessing(err, reject);

                let data = testQuestData.toObject();

                this.__getEventManager().dispatch({
                    type: events.TEST_QUESTION_CREATED,
                    data: data
                });

                resolve(data);
            }).catch(err => {
                this.__errorProcessing(err, reject);
            });
        });
    }

    updateTestQuestion(testQuestion, qId, testId)
    {
        return new Promise((resolve, reject) => {
            if(!qId || !testId)
            {
                throw new ValidationError("id is mandatory field");
            }
            new TestModel().updateQuestion(testQuestion, qId, testId).then( (testQuestData, err) => {
                if(err) return this.__errorProcessing(err, reject);
                if(!testQuestData)
                {
                    return reject(new NotFoundEntityException());
                }
                let data = testQuestData.toObject();

                this.__getEventManager().dispatch({
                    type: events.TEST_QUESTION_UPDATED,
                    data: data
                });

                resolve(data);
            }).catch(err => {
                this.__errorProcessing(err, reject);
            });
        });
    }

    removeTestQuestion(testQuestionId, testId)
    {
        return new Promise((resolve, reject) => {
            if(!testQuestionId || !testId)
            {
                throw new ValidationError("id is mandatory field");
            }
            new TestModel().removeQuestion(testQuestionId, testId).then( (writeOpResult, err) => {
                if(err) return this.__errorProcessing(err, reject);

                if (!writeOpResult) {
                    return reject(new NotFoundEntityException());
                }

                this.__getEventManager().dispatch({
                    type: events.TEST_QUESTION_DELETED,
                    data: {id: testQuestionId}
                });

                resolve(true);
            }).catch(err => {
                this.__errorProcessing(err, reject);
            });
        });
    }

    addUserDataFilter(userData)
    {
        this.userTofilter = userData;
        return this;
    }

    __getGroupModel()
    {
        return new GroupModel().addAdditionalConditions(this.userTofilter);
    }

    __getTestModel()
    {
        return new TestModel().addAdditionalConditions(this.userTofilter);
    }

    __errorProcessing(err, reject)
    {
        console.log(err.errmsg);
        if(err.code === 11000)
        {
            reject( new EntityDuplicateKeyException(err.errmsg));
        }
        reject( new InternalError(err.errmsg) );
    }

    __getEventManager()
    {
        return  serviceContainer.getService('internal', 'eventService');
    }
}

module.exports = TestService;
