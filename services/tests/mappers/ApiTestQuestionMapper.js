const Mapper = require("../../../libs/Mapper");
const ValidationError = require("../exceptions/ValidationError");

class ApiTestQuestionMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "question",
                toClient: "question",
                type: "string",
            },
            {
                toServer: "variants",
                toClient: "options",
                type: "array",
                toServerFunc: ( (variants, object) => {
                    let uniquesConstrain = {};
                    variants.map(variant => {
                        if(typeof variant.aid === "undefined" || typeof variant.value === "undefined")
                        {
                            throw new ValidationError("eachVariant should have aid and value");
                        }

                        if(uniquesConstrain[variant.aid])
                        {
                            throw new ValidationError("Duplicate values of aid");
                        }
                        uniquesConstrain[variant.aid]  = 1;
                        return variant;
                    });

                    if(!object.answers || !object.answers.length)
                    {
                        throw new ValidationError("Duplicate values of aid");
                    }

                    object.answers.map(answer => {
                        if(!uniquesConstrain[answer])
                        {
                            throw new ValidationError("Answers contains not exists aid value");
                        }
                    });

                    return variants;
                })

            },
            {
                toServer: "answers",
                toClient: "answers",
                type: "array",
            },
            {
                toServer: "guiType",
                toClient: "guiType",
                type: "string",
                toServerFunc: (type => {
                    if(['radio', 'checkbox'].indexOf(type) == -1)
                    {
                        throw new ValidationError("guiType should be one of ['radio', 'checkboxes']")
                    }
                    return type;
                })
            },
            {
                toServer: "_id",
                toClient: "qid",
                type: "string",
            },
        ];
    }
}

module.exports = new ApiTestQuestionMapper();