const mongoose = require('../../../libs/mongoose').mongoose;
const ModelValidationError = require('../exceptions/ModelValidationError');

// define the testQuestion model schema
const TestQuestionsSchema = new mongoose.Schema({
    question: {type: String, default: ''},
    variants: {type: Array, default: []},
    answers: {type: Array, default: []},
    guiType: {type: String, enum: ['radio', 'checkbox']},
});


class TestQuestionsModel
{

    getSchema()
    {
        return TestQuestionsSchema;
    }

    validateData(question)
    {
        if(!question.question || !question.question.trim())
        {
            throw new ModelValidationError("Question should not be empty");
        }
        if(!question.answers || !Array.isArray(question.answers))
        {
            throw new ModelValidationError("answers should be array");
        }
        if(!question.variants || !Array.isArray(question.variants))
        {
            throw new ModelValidationError("variants should be array");
        }

        let keys = [];
        let mappedAids = {};

        question.variants.map( variant => {
            if(typeof variant.aid === "undefined" || typeof variant.value === "undefined")
            {
                throw new ModelValidationError("eachVariant should have aid and value");
            }

            if(mappedAids[variant.aid])
            {
                throw new ModelValidationError("Duplicate values of aid");
            }
            mappedAids[variant.aid]  = 1;
        });

        for(let an of question.answers)
        {
            if(!mappedAids[an])
            {
                throw new ModelValidationError("answer not in variants");
            }

            if(keys.indexOf(an) > -1)
            {
                throw new ModelValidationError("duplicate answer");
            }
            keys.push(an);
        }
    }
}

module.exports = new TestQuestionsModel();