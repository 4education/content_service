const communicatorService = require("../CommunicatorService");
const CoreApi = require("../../core/entryPoints/Api");

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/internal/test_url/:tid": {
                    act: this.__getMethod('createTestLinkData')
                },
                "/internal/tests/:tid/publish_conf": {
                    act: this.__getMethod('getTestPublishCong')
                },
                "/internal/test_theme/:tid": {
                    act: this.__getMethod('getTestTheme')
                }
            },
            "post": {
                "/internal/createUserDefaultData/": {
                    act: this.__getMethod('createUserDefaultData')
                },
            }
        };
    }

    async createTestLinkData(req, res)
    {
        let tokenData = await new communicatorService().createToken(req.params.tid);

        res.status(200).send(
            tokenData
        );
    }

    async getTestPublishCong(req, res)
    {
        let testData = await new communicatorService().getTestPublishCong(req.params.tid);

        res.status(200).send(
            testData
        );
    }

    async getTestTheme(req, res)
    {
        let testData = await new communicatorService().getTestTheme(req.params.tid);

        res.status(200).send(
            testData
        );
    }

    async createUserDefaultData(req, res)
    {
        let result = await new communicatorService().createUserDefaultData(req.body.cid, req.body.uid);

        res.status(200).send(
            result
        );
    }
}

module.exports = Api;