const jwt = require("jsonwebtoken");
const tokenConf = require("./config/token");
const serviceContainer = require("../../libs/ServiceContainer");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
// define the UserToken model schema
// const UserTokenSchema = new mongoose.Schema({
//     uid: {
//         type: String,
//         index: { unique: true }
//     },
//     token: String
// });
//
// const provider = mongoose.model('users_tokens', UserTokenSchema);

class CommunicatorService
{
    async createToken(testId)
    {
        const data = await this.getTestPublishCong(testId);
        let token = jwt.sign(data, tokenConf.secretKey, {
            expiresIn: tokenConf.expiresIn
        });

        return {token, url: tokenConf.launch_service_url};
    }

    getTestEncryptData(testData, publishData)
    {
        return {
            v:              testData.version,
            tid:            testData._id,
            publishData:    publishData.settings || {}
        };
    }

    async getTestPublishCong(testId)
    {
        const testData = await serviceContainer.getService('internal', 'testsService').getTestById(testId);
        const publishData = await serviceContainer.getService('internal', 'publishConfig').getById(testData.pid);

        if(!testData || !testData.isActive)
        {
            throw new NotFoundEntityException("test not found");
        }

        return this.getTestEncryptData(testData, publishData);
    }

    async getTestTheme(testId)
    {
        const testData = await serviceContainer.getService('internal', 'testsService').getTestById(testId);

        if(!testData || !testData.isActive)
        {
            throw new NotFoundEntityException("test not found");
        }
        return serviceContainer.getService('internal', 'themes').getById(testData.thid);
    }

    // async createUserDefaultData(cid, uid)
    // {
    //     return new InstallFixture().run(cid, uid);
    // }
}
module.exports = CommunicatorService;
