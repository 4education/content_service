const Mapper = require("../../../libs/Mapper");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "thid",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "name",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean"
            },
            {
                toServer: "description",
                toClient: "description",
                type: "string",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "styles",
                toClient: "style",
                type: "object"
            }
        ];
    }
}

module.exports = ApiMapper;
