let serviceContainer = require("./libs/ServiceContainer");
//======================================================================================================================

//router Service
let routerService = require("./services/router/RouterService");
serviceContainer.registerServiceEntryPoints('internal', 'routerService', routerService);
//======================================================================================================================


//event service
let eventPointInternal = require("./services/event/entryPoints/Internal");
serviceContainer.registerServiceEntryPoints('internal', 'eventService',eventPointInternal);
//======================================================================================================================

//publishing conf  services
// let testsApi = require("./services/tests/entryPoints/Api");
// let internalApi = require("./services/tests/entryPoints/InternalApi");
// let eventApi = require("./services/tests/entryPoints/EventApi");
//
// serviceContainer.registerServiceEntryPoints('api', 'testsService', new testsApi());
// serviceContainer.registerServiceEntryPoints('internal', 'testsService', new internalApi());
// serviceContainer.registerServiceEntryPoints('events', 'testsService', new eventApi());
//======================================================================================================================

//publishing conf  services
// let pbConf = require("./services/publishConfig/entryPoints/InternalApi");
// let pbConfApi = require("./services/publishConfig/entryPoints/Api");
// let pbEventApi = require("./services/publishConfig/entryPoints/EventApi");
//
// serviceContainer.registerServiceEntryPoints('api', 'publishConfig', new pbConfApi());
// serviceContainer.registerServiceEntryPoints('internal', 'publishConfig', new pbConf());
// serviceContainer.registerServiceEntryPoints('events', 'publishConfig', new pbEventApi());
//======================================================================================================================

//themes conf  services
// let themesIntApi = require("./services/themes/entryPoints/InternalApi");
// let themesApi = require("./services/themes/entryPoints/Api");
// let themesEventApi = require("./services/themes/entryPoints/EventApi");
//
//
// serviceContainer.registerServiceEntryPoints('api', 'themes', new themesApi());
// serviceContainer.registerServiceEntryPoints('internal', 'themes', new themesIntApi());
// serviceContainer.registerServiceEntryPoints('events', 'themes', new themesEventApi());
//======================================================================================================================

//internalCommunicator services
// let communicatorApi = require("./services/internalCommunicator/entryPoints/Api");
//
// serviceContainer.registerServiceEntryPoints('api', 'internalCommunicator', new communicatorApi());
// let communicatorInternal = require("./services/internalCommunicator/entryPoints/InternalApi");
//
// serviceContainer.registerServiceEntryPoints('internal', 'internalCommunicator', new communicatorInternal());
//======================================================================================================================

//notification service
let notificationPointInternal = require("./services/notification/entryPoints/Internal");
let emailConf = require("./config/email");
serviceContainer.registerServiceEntryPoints('internal', 'notificationService', new notificationPointInternal(emailConf));
//======================================================================================================================

//statistic service
// let statIntApi = require("./services/statistic/entryPoints/InternalApi");
// let statApi = require("./services/statistic/entryPoints/Api");
// let statEventApi = require("./services/statistic/entryPoints/EventApi");
//
//
// serviceContainer.registerServiceEntryPoints('api', 'statistic', new statApi());
// serviceContainer.registerServiceEntryPoints('internal', 'statistic', new statIntApi());
// serviceContainer.registerServiceEntryPoints('events', 'statistic', new statEventApi());
//======================================================================================================================


//content services
 let contentApi = require("./services/content/entryPoints/Api");
 // let internalApi = require("./services/content/entryPoints/InternalApi");
 // let eventApi = require("./services/content/entryPoints/EventApi");

 serviceContainer.registerServiceEntryPoints('api', 'contentService', new contentApi());
 // serviceContainer.registerServiceEntryPoints('internal', 'testsService', new internalApi());
 // serviceContainer.registerServiceEntryPoints('events', 'testsService', new eventApi());
//======================================================================================================================

//users services
let UsersApi = require("./services/users/entryPoints/InternalApi");

serviceContainer.registerServiceEntryPoints('internal', 'usersService', new UsersApi());
//======================================================================================================================


//security service
let oauthInternal = require("./services/oauth/entryPoints/InternalApi");
routerService.setMiddleWareFunction(new oauthInternal().addUserDataToRequest);

let securityService = require("./services/security/entryPoints/Api");
routerService.setMiddleWareFunction(new securityService().processRequest);
//======================================================================================================================

