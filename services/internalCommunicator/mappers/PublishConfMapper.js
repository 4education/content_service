const Mapper = require("../../../libs/Mapper");

class PublishConfMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "pid",
                type: "string",
            },
            {
                toServer: "label",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean"
            },
            {
                toServer: "description",
                toClient: "description",
                type: "string",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "settings",
                toClient: "settings",
                toServerFunc: (settings => {
                    if(settings.gather && settings.gather.length > 0)
                    {
                        const validValues = ['email', 'phone', 'name', 'skype'];
                        const res = settings.gather.filter( item => { return validValues.indexOf(item) !== -1 });

                        if(res.length != settings.gather.length)
                        {
                            throw new ValidationError("settings.gather have not valid value. Valid values : 'email', 'phone', 'name', 'skype'");
                        }
                    }
                    return settings;
                }),
                type: "object"
            }
        ];
    }
}

module.exports = PublishConfMapper;