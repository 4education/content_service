class InternalError
{
    constructor(dataObject) {
        this.message = dataObject.message;
        this.code = dataObject.code;
    }
}
module.exports = InternalError;