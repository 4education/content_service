module.exports = {
    db:"mongodb", //this is static data for connection using in mongoose library
    host:process.env.CONTENT_SERVICE_DB_HOST,
    port: process.env.CONTENT_SERVICE_DB_PORT,
    dbName:process.env.CONTENT_SERVICE_DB_NAME
};
