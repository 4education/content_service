const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the courses model schema
const CourseSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    description: {
        type: String
    },
    titleImageUrl: {
        type: String
    },
    // catId: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "content_categories",
    //     index: true
    // },
    modulesCount: {type: Number, default: 0},
    cid: { type: mongoose.Schema.Types.ObjectId, index: true }, //company_id
    updatedDate:{ type: Date, default: null},
    createdDate:{ type: Date, default: Date.now},
});

const provider = mongoose.model('content_courses', CourseSchema);

class CourseModel extends FilteredCollectionModel
{
    async create(data)
    {
        let categoryData = {
            name: data.name,
            description: data.description,
            titleImageUrl: data.titleImageUrl,
            //catId: data.catId,
            cid: data.cid
        };
        return provider.create(categoryData);
    }

    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    updateItemById(data, id)
    {
        this.validateMongoDBId(id);

        data.updatedDate = Date.now();

        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), {$set: data}, {new: true}).exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }

    getAll()
    {
        return provider.find().lean().exec();
    }

    removeById(id)
    {
        this.validateMongoDBId(id);
        return provider.find({_id: mongoose.Types.ObjectId(id)}).remove().exec();
    }
}

module.exports = CourseModel;
