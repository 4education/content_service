const PublishConfigService = require("../PublishConfigService");


class InternalApi
{
    getById(id)
    {
        return new PublishConfigService().getItem(id);
    }
}

module.exports = InternalApi;