const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');
const MaterialSection = require('./MaterialSection');

// define the Lesson model schema
const LessonSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    moduleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "content_modules",
        index: true
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "content_courses",
        index: true
    },
    materialItems: {type: [MaterialSection.getSchema()]},
    cid: { type: mongoose.Schema.Types.ObjectId, index: true }, //company_id
    updatedDate:{ type: Date, default: null},
    createdDate:{ type: Date, default: Date.now},
});

const provider = mongoose.model('content_lesson', LessonSchema);

class LessonModel extends FilteredCollectionModel
{
    async create(data)
    {
        let categoryData = {
            name: data.name,
            moduleId: data.moduleId,
            courseId: data.courseId,
            materialItems: data.materialItems,
            cid: data.cid
        };
        return provider.create(categoryData);
    }

    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    updateItemById(data, id)
    {
        this.validateMongoDBId(id);

        data.updatedDate = Date.now();

        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), {$set: data}, {new: true}).exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }

    getAll()
    {
        return provider.find().lean().exec();
    }

    removeById(id)
    {
        this.validateMongoDBId(id);
        return provider.find({_id: mongoose.Types.ObjectId(id)}).remove().exec();
    }

    createSection(materialSec, id)
    {
        MaterialSection.validateData(materialSec);
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}),
            { $push: {"materialItems": materialSec}, $inc: { version: 1 }},{new: true}).exec();
    }

    updateSection(materialSec, sid, id)
    {
        MaterialSection.validateData(materialSec);
        return provider.update(
            this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id), "questions._id": mongoose.Types.ObjectId(sid)}),
            { $set: {"materialItems.$": materialSec}, $inc: { version: 1 }},{new: true}).exec();
    }

    removeSection(sId, id)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}),
            { $pull: {"materialItems": { "_id": mongoose.Types.ObjectId(sId)}}},{new: true}).exec();
    }
}

module.exports = LessonModel;
