const publishFixture = require("./fixtures/publish_confs.json");
const themesFixtures = require("./fixtures/themes.json");
const testsFixtures =  require("./fixtures/tests.json");
const configurationFixtures =  require("./fixtures/configuration.json");
const groupsFixture = require("./fixtures/groups.json");

const testService = require("../services/tests/TestService");
const configurationService = require("../services/configuration/ConfigurationDataService");
const PublishService = require("../services/publishConfig/PublishConfigService");
const ThemesService = require("../services/themes/ThemesService");
const mongoose = require("../libs/mongoose").mongoose;


class InstallFixture
{
    async run(cid, uid) {
        //clear db first;
        if(!cid || !uid)
        {
            await mongoose.connection.db.dropDatabase();
        }
        const publishingItems = await this.createPublishingConf(cid, uid);
        const themesItems = await this.createThemes(cid, uid);
        const publishingId = publishingItems[0]._id.toString();
        const themaId = themesItems[0]._id.toString();
        const tests = await this.createTests(publishingId, themaId, cid, uid);


        const promises = groupsFixture.map(async (group) => {
            group.pid = publishingId;
            group.thid   = themaId;
            group.testsList = tests;
            if(cid && uid)
            {
                group.cid = cid;
                group.uid = uid;
            }

            let gr = await new testService().createGroup(group);
            if(!gr)
            {
                throw Error(group.name+" failed due to creation");
            }
        });
        await Promise.all(promises).catch(e => console.log(e));
    }

    async createPublishingConf(cid, uid)
    {
        const collection = publishFixture.map(async (item) => {
            if(cid && uid)
            {
                item.cid = cid;
                item.uid = uid;
            }
            let publish = await new PublishService().create(item);
            if(!publish)
            {
                throw Error(item.label+" failed due to creation");
            }
            return publish;
        });

        return await Promise.all(collection).then(res => { return res;}).catch(e => console.log(e));
    }

    async createThemes(cid, uid)
    {
        const collection = themesFixtures.map(async (item) => {
            if(cid && uid)
            {
                item.cid = cid;
                item.uid = uid;
            }
            let publish = await new ThemesService().create(item);
            if(!publish)
            {
                throw Error(item.label+" failed due to creation");
            }
            return publish;
        });

        return await Promise.all(collection).then(res => { return res;}).catch(e => console.log(e));
    }

    async createTests(pid, thid, cid, uid)
    {
        const collection = testsFixtures.map(async (item) => {
            if(cid && uid)
            {
                item.cid = cid;
                item.uid = uid;
            }
            item.thid = thid;
            item.pid = pid;
            let test = await new testService().createTest(item);
            if(!test)
            {
                throw Error(item.label+" failed due to creation");
            }
            return test._id;
        });

        return await Promise.all(collection).then(res => { return res;}).catch(e => console.log(e));
    }

    async createConfiguration()
    {
        const collection = configurationFixtures.map(async (item) => {

            let test = await new configurationService().createDefault(item);
            if(!test)
            {
                throw Error(item.label+" failed due to creation");
            }
            return test._id;
        });

        return await Promise.all(collection).then(res => { return res;}).catch(e => console.log(e));
    }
}
module.exports = InstallFixture;
