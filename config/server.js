const {
	NODE_ENV,
	CONTENT_SERVICE_HOST,
	CONTENT_SERVICE_PORT,
	LAUNCH_SERVICE_HOST,
	LAUNCH_SERVICE_PORT
} = process.env;

module.exports = {
	"serviceName": "educational",
	"protocol": NODE_ENV == 'production' ? 'https://' : 'http://',
    "host": CONTENT_SERVICE_HOST,
    "port": CONTENT_SERVICE_PORT,
	"origin": `${CONTENT_SERVICE_HOST}${CONTENT_SERVICE_PORT ? `:${CONTENT_SERVICE_PORT}` : ''}`,
	"launcher_origin": `${LAUNCH_SERVICE_HOST}${LAUNCH_SERVICE_PORT ? `:${LAUNCH_SERVICE_PORT}` : ''}`
};