const InternalError = require("./InternalError");

class RoleDontHavePermissionError extends InternalError
{

}

module.exports = RoleDontHavePermissionError;