const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the courses model schema
const AssignedUsersSchema = new mongoose.Schema({
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "content_courses",
        index: true
    },
    users: [{type: mongoose.Schema.Types.ObjectId, index: true, unique: true}],
    usersGroups: [{type: mongoose.Schema.Types.ObjectId, index: true, unique: true}],
    updatedDate:{ type: Date, default: null},
    createdDate:{ type: Date, default: Date.now},
});

const provider = mongoose.model('content_assigned_users', AssignedUsersSchema);

class AssignedUsersModel extends FilteredCollectionModel
{
    async create(data)
    {
        let categoryData = {
            courseId: data.courseId,
            users: data.users,
            usersGroups: data.usersGroups
        };
        return provider.create(categoryData);
    }

    __getLookup()
    {
        return {from: 'content_courses', localField: 'courseId', foreignField: '_id', as: 'course'};
    }

    getProvider()
    {
        return provider;
    }

    updateItemById(data, id)
    {
        this.validateMongoDBId(id);

        data.updatedDate = Date.now();

        return provider.findOneAndUpdate(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)}), {$set: data}, {new: true}).exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)})).exec();
    }

    getAll()
    {
        return provider.find().lean().exec();
    }

    removeById(id)
    {
        this.validateMongoDBId(id);
        return provider.find({courseId: mongoose.Types.ObjectId(id)}).remove().exec();
    }

    addUser(uid, id)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)}),
            { $push: {"users":  mongoose.Types.ObjectId(uid)}},{new: true, upsert: true}).exec();
    }

    removeUser(uid, id)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)}),
            { $pull: {"users": mongoose.Types.ObjectId(uid)}}, {new: true, upsert: true}).exec();
    }

    addUserGroup(guid, id)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)}),
            { $push: {"usersGroups":  mongoose.Types.ObjectId(guid)}}, {new: true, upsert: true}).exec();
    }

    removeUserGroup(guid, id)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({courseId: mongoose.Types.ObjectId(id)}),
            { $pull: {"usersGroups":  mongoose.Types.ObjectId(guid)}}, {new: true, upsert: true}).exec();
    }
}

module.exports = AssignedUsersModel;
