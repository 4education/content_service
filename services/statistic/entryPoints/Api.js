const statisticService = require("../StatisticService");
const CoreApi = require("../../core/entryPoints/Api");
const ApiMapper = require("../mappers/ApiMapper");

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/statistic": {
                    act: this.__getMethod('getAll')
                },
            },
        };
    }

    /**
     * Returns todolist collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getAll(req, res)
    {
        let list = await new statisticService().getAll();
        if(!list)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapCollectionToClient(list)
        );
    }
}

module.exports = Api;
