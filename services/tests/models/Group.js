const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the group model schema
const GroupSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    thid: { type: mongoose.Schema.Types.ObjectId, index: true },
    pid: { type: mongoose.Schema.Types.ObjectId, index: true },
    isActive: {type: Boolean, default: false},
    testsCount: {type: Number, default: 0},
    createdDate:{ type: Date, default: Date.now},
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    cid: { type: mongoose.Schema.Types.ObjectId, index: true }, //company_id
    updatedDate:{ type: Date, default: null}
});

GroupSchema.post('remove', (doc, next) => {
    console.log('%s has been removed', doc._id);
    this.model('testing_tests').update(
        {},
        {$pull: {groupsList: doc._id}},
        {multi: true},
        next
    );
});
const provider = mongoose.model('tests_groups', GroupSchema);

class GroupModel extends FilteredCollectionModel
{
    async create(data)
    {
        let categoryData = {
            name: data.name,
            isActive: data.isActive === true,
            testsCount: data.testsCount || 0,
            pid: data.pid,
            uid: data.uid,
            cid: data.cid,
        };

        if(data.thid)
        {
            categoryData.thid = data.thid;
        }
        return provider.create(categoryData);
    }

    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    updateItemById(data, id)
    {
        this.validateMongoDBId(id);

        data.updatedDate = Date.now();

        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), {$set: data}, {new: true}).exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }

    getAll()
    {
        return provider.find().lean().exec();
    }

    removeById(id)
    {
        this.validateMongoDBId(id);
        return provider.find({_id: mongoose.Types.ObjectId(id)}).remove().exec();
    }

    getTestGroupsFilteredCollection(requestObject, testId)
    {
        requestObject.addWhereConditionsForCustomObject('testsList',{$elemMatch: {tid: mongoose.Types.ObjectId(testId)}});
        return this.executeQuery(requestObject);
    }

    isAllGroupsExists(gids)
    {
        return new Promise((resolve, reject) => {
            provider.find(this.getAdditionalConditions({_id: {$in: gids}})).countDocuments().exec().then(itemsCount => {
                resolve(gids.length == itemsCount);
            }).catch(err => {
                resolve(false)
            })
        });
    }

    addTestToGroups(grIds)
    {
        return provider.updateMany(
            {_id: {$in: grIds} },
            {$inc: { testsCount: 1 }}
        );
    }

    removeTestFromGroups(grIds)
    {
        return provider.updateMany(
            {_id: {$in: grIds} },
            {$inc: { testsCount: -1 }}
        );
    }
}

module.exports = GroupModel;
