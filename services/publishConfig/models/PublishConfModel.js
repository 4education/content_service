const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the User model schema
const PublishConfSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    settings: {
        report: {type: String, enum: ['email', 'internal']},
        allowance: {type: String, enum: ['one_step', 'allow_recheck', 'do_not_allow']},
        results: {type: String, enum: ['full', 'full_without_right_answers', 'short']},
        gather: {type: Array, default: []},
		privacy: {type: String, enum: ['public', 'link', 'email']},
		email: String,
		socials: { type: Object }
    },
    isActive: Boolean,
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    cid: { type: mongoose.Schema.Types.ObjectId, index: true }, //company_id
    createdDate:{ type: Date, default: Date.now},
    updatedDate:{ type: Date, default: null}
});

const provider = mongoose.model('publish_confs', PublishConfSchema);

class PublishConfModel extends FilteredCollectionModel
{
    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    create(itemData)
    {
        let data = {
            name: itemData.name,
            settings: itemData.settings,
            isActive: itemData.isActive || false,
            uid: itemData.uid,
            cid: itemData.cid,
        };
        return provider.create(data);
    }

    update(id, itemData)
    {
        itemData.updatedDate = Date.now();
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), {$set: itemData}, {new: true}).exec();
    }

    getAll(callback, paggingData = null)
    {
        return provider.find(this.getAdditionalConditions()).lean().exec();
    }

    remove(id)
    {
        this.validateMongoDBId(id);
        return provider.find(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).remove().exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }
}

module.exports = PublishConfModel;
