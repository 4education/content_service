const protocol = process.env.NODE_ENV == 'production' ? 'https://' : 'http://';
module.exports = {
    "protocol": protocol,
    "host":process.env.USERS_SERVICE_HOST,
    "port":process.env.USERS_SERVICE_PORT,
    "url": protocol+process.env.USERS_SERVICE_HOST+ (process.env.USERS_SERVICE_PORT ? ':'+process.env.USERS_SERVICE_PORT : '') +'/api',
    "secret_key_header_name": process.env.SECRET_KEY_HEADER_NAME,
    "secret_key_communication": process.env.SECRET_KEY_COMMUNICATION
};