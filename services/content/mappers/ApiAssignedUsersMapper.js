const Mapper = require("../../../libs/Mapper");

class ApiAssignedUsersMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "courseId",
                toClient: "courseId",
                type: "string",
                required: true,
            },
            {
                toServer: "users",
                toClient: "users",
                type: "array",
            },
            {
                toServer: "uid",
                toClient: "uid",
                type: "string",
            },
            {
                toServer: "guid",
                toClient: "guid",
                type: "string",
            },
            {
                toServer: "usersGroups",
                toClient: "usersGroups",
                type: "array",
            },
            {
                toServer: "course",
                toClient: "course",
                toServerSkipped: true,
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "version",
                toClient: "version",
                toServerSkipped: true,
                type: "string",
            },
        ];
    }
}

    module.exports = ApiAssignedUsersMapper;
