const mongoose = require('../../../libs/mongoose').mongoose;
const ModelValidationError = require('../exceptions/ModelValidationError');
const TestQuestions = require('./TestQuestions');
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');


// define the test model schema
const TestSchema = new mongoose.Schema({
    name: String,
    description: String,
    thid: { type: mongoose.Schema.Types.ObjectId, index: true },
    pid: { type: mongoose.Schema.Types.ObjectId, index: true },
    isActive: {type: Boolean, default: false},
    isTimer: {type: Boolean, default: false},
    timer: {type: Number, default: 0}, // in seconds
    complexity: {type: Number},
    questions: {type: [TestQuestions.getSchema()]},
    minPassing: {type: Number, default: 50},
    groupsList:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: "tests_groups",
        index: true
    }],
    createdDate:{ type: Date, default: Date.now},
    updatedDate:{ type: Date, default: null},
    version: {type: Number, default: 0},
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    cid: { type: mongoose.Schema.Types.ObjectId, index: true } //company_id
});

// TestSchema.post('remove', (doc, next) => {
//     console.log('test %s has been removed', doc._id);
//     this.model('tests_groups').update(
//         {_id: {$in: doc.groupsList} },
//         {$inc: { testsCount: -1 }},
//         {multi: true},
//         next
//     );
// });

const provider = mongoose.model('testing_tests', TestSchema);

class TestModel extends FilteredCollectionModel
{

    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    create(test)
    {
        let testData = {
            name: test.name,
            isActive: true, //test.isActive === true,
            isTimer: test.isTimer === true,
            timer: test.timer
        };

        if(test.questions)
        {
            testData.questions = test.questions;
        }

        if(test.description)
        {
            testData.description = test.description;
        }

        if(test.minPassing)
        {
            testData.minPassing = test.minPassing;
        }

        if(test.complexity)
        {
            testData.complexity = test.complexity;
        }

        if(test.pid)
        {
            testData.pid = test.pid;
        }

        if(test.thid)
        {
            testData.thid = test.thid;
        }

        if(test.groupsList)
        {
            testData.groupsList = test.groupsList;
        }

        if(test.uid)
        {
            testData.uid = test.uid;
        }

        if(test.cid)
        {
            testData.cid = test.cid;
        }

        this.validateData(testData);
        return provider.create(testData);
    }

    validateData(test)
    {
        if(!test.name || !test.name.trim())
        {
            throw new ModelValidationError("Name of test should not be empty");
        }
        if( typeof test.isActive !== "boolean")
        {
            throw new ModelValidationError("isActive should be boolean");
        }
        if( typeof test.isTimer !== "boolean")
        {
            throw new ModelValidationError("isTimer should be boolean");
        }
        if(test.isTimer === true)
        {
            if(test.timer <= 0)
            {
                throw new ModelValidationError("timer should be more then 0");
            }
        }

        if(test.questions)
        {
            for(let item of test.questions)
            {
                TestQuestions.validateData(item);
            }
        }
    }

    getTest(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(
            this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})
        ).populate('groupsList', 'name').exec();
    }

    getTests()
    {
        return provider.find().lean().exec();
    }

    removeTest(id)
    {
        return provider.find(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).remove().exec();
    }

    updateTestById(test, id)
    {

        let testData = {
            name: test.name,
            isActive: true,//test.isActive === true,
            isTimer: test.isTimer === true,
            timer: test.timer
        };

        if(test.questions)
        {
            testData.questions = test.questions;
        }

        if(test.description)
        {
            testData.description = test.description;
        }

        if(test.complexity)
        {
            testData.complexity = test.complexity;
        }

        if(test.pid)
        {
            testData.pid = test.pid;
        }

        if(test.thid)
        {
            testData.thid = test.thid;
        }

        if(test.minPassing)
        {
            testData.minPassing = test.minPassing;
        }

        testData.updatedDate = Date.now();

        return provider.findOneAndUpdate(
            this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}),
            {$set: testData, $inc: { version: 1 }},
            {new: true}
            ).exec();
    }

    createQuestion(question, id)
    {
        TestQuestions.validateData(question);
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), { $push: {"questions": question}, $inc: { version: 1 }},).exec();
    }

    updateQuestion(testQuestion, qId, testId)
    {
        TestQuestions.validateData(testQuestion);
        return provider.update(
            this.getAdditionalConditions({_id: mongoose.Types.ObjectId(testId), "questions._id": mongoose.Types.ObjectId(qId)}),
            { $set: {"questions.$": testQuestion}, $inc: { version: 1 }},).exec();
    }

    removeQuestion(qId, testId)
    {
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(testId)}),
            { $pull: {"questions": { "_id": mongoose.Types.ObjectId(qId)}}, $inc: { version: 1 }},).exec();
    }

    isAllTestsExists(tids)
    {
        return new Promise((resolve, reject) => {
            provider.find(this.getAdditionalConditions({_id: {$in: tids}})).countDocuments().exec().then(itemsCount => {
                resolve(tids.length == itemsCount);
            }).catch(err => {
                resolve(false)
            })
        });
    }

    async addGroupToTests(testsIds, grId)
    {
        return provider.updateMany({_id: {$in: testsIds} }, { $push: { groupsList: grId }});
    }

    async removeGroupFromTests(grId)
    {
        let d = await provider.find( { groupsList: grId }).exec();
        console.log(d);
        return provider.updateMany( { groupsList: grId }, { $pull: { groupsList: grId }})
    }
}

module.exports = TestModel;
