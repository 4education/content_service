const async = require("async");
const serviceContainer = require("../../../libs/ServiceContainer");
const emailConfigs = require("../../../config/email");

class Api
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "post": {
                "/send_email_to_admin": {
                    act: this.__getMethod('sendEmailToAdmin')
                }

            }
        }
    }

    __getMethod(name)
    {
        let method = name;
        return (req, res) => {
            try{
                let ctrlMethod = this[method](req, res);
                if(ctrlMethod && ctrlMethod.catch)
                {
                    ctrlMethod.catch(exception => {
                        this.__sendFailedResponse(req, res, exception);
                    })
                }
            }
            catch(exception)
            {
                this.__sendFailedResponse(req, res, exception);
            }
        };
    }

    /**
     * Returns success or error
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async sendEmailToAdmin(req, res)
    {
        if(!req.body.fromEmail || !req.body.subject || !req.body.text)
        {
            return res.status(400).send(
                {message:"Mandatory parameters are missing", code:"email.1"}
            );
        }
        serviceContainer.getService('internal', 'notificationService')
            .sendMailToAdmin(
                req.body.fromEmail,
                req.body.subject,
                req.body.text
            )
            .then(data => {
                res.status(200).send(
                    {message:"success"}
                );
            })
            .catch(err => {
                res.status(500).send(
                    {message:"failed", code:"email.0"}
                );
            });
    }

    __sendFailedResponse(req, res, exception)
    {
        res.status(exceptionMapper.getApiStatus(exception)).send(
            { message: exception.message,  code:"email.0" }
        );
        console.log(exception, exception.stack.split("\n"));
    }
}

module.exports = new Api();
