const Mapper = require("../../../libs/Mapper");
const ValidationError = require("../exceptions/ValidationError");

class ApiMaterialsMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "sid"
            },
            {
                toServer: "attributes",
                toClient: "attributes"
            },
            {
                toServer: "type",
                toClient: "type",
                type: "string",
                toServerFunc: (type => {
                    if(['text','image_text','video_text','test_radio_button','test_checkbox','test_image'].indexOf(type) === -1)
                    {
                        throw new ValidationError("guiType should be one of ['text','image_text','video_text','test_radio_button','test_checkbox','test_image']")
                    }
                    return type;
                })
            }
        ];
    }
}

module.exports = new ApiMaterialsMapper();