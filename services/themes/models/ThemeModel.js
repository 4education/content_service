const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the User model schema
const ThemesSchema = new mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    styles: { type: Object, default:{} },//mongoose.Schema.Types.Mixed,
    isActive: Boolean,
    createdDate:{ type: Date, default: Date.now},
    updatedDate:{ type: Date, default: null},
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    cid: { type: mongoose.Schema.Types.ObjectId, index: true } //company_id
});

const provider = mongoose.model('themes', ThemesSchema);

class ThemeModel extends FilteredCollectionModel
{
    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    create(itemData)
    {
        let data = {
            name: itemData.name,
            styles: itemData.styles,
            isActive: itemData.isActive || false,
            uid: itemData.uid,
            cid: itemData.cid,
        };
        return provider.create(data);
    }

    update(id, itemData)
    {
        itemData.updatedDate = Date.now();
        return provider.findOneAndUpdate(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)}), {$set: itemData}, {new: true}).exec();
    }

    getAll(callback, paggingData = null)
    {
        return provider.find(this.getAdditionalConditions()).lean().exec();
    }

    remove(id)
    {
        this.validateMongoDBId(id);
        return provider.find(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).remove().exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }
}

module.exports = ThemeModel;
