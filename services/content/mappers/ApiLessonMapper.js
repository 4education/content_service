const Mapper = require("../../../libs/Mapper");
const ApiMaterialsMapper = require("./ApiMaterialsMapper");

class ApiLessonMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "id",
                type: "string",
                toServerSkipped: true,
            },
            {
                toServer: "name",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "moduleId",
                toClient: "moduleId",
                type: "string",
                required: true,
            },
            {
                toServer: "courseId",
                toClient: "courseId",
                type: "string",
                required: true,
            },
            {
                toServer: "materialItems",
                toClient: "materialItems",
                toServerFunc: (items => {
                    if(items && items.length && items.length > 0)
                    {
                        return items.map( item => {
                            return ApiMaterialsMapper.toServerProperty(item);
                        })
                    }
                    return items;
                }),
                toClientFunc: (items => {
                    if(items && items.length && items.length > 0)
                    {
                        return items.map( item => {
                            return ApiMaterialsMapper.toClientProperty(item);
                        })
                    }
                    return items;
                }),
                type: "array",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "statData",
                toClient: "statData",
                toServerSkipped: true,
            },
        ];
    }
}

    module.exports = ApiLessonMapper;
