const testService = require("../TestService");
const serviceContainer = require("../../../libs/ServiceContainer");
const ApiGroupMapper = require("../mappers/ApiGroupMapper");
const ApiTestsMapper = require("../mappers/ApiTestMapper");
const ApiTestQuestionMapper = require("../mappers/ApiTestQuestionMapper");
const CoreApi = require("../../core/entryPoints/Api");
const { protocol, launcher_origin } = require("../../../config/server");
const path = require('path');

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/tests_groups": {
                    act: this.__getMethod('getGroups')
                },
                "/tests_groups/:id": {
                    act: this.__getMethod('getGroup')
                },
                "/tests/": {
                    act: this.__getMethod('getTests')
                },
                "/tests/:id": {
                    act: this.__getMethod('getTest')
                },
                //internal
                "/internal/tests/:id": {
                    act: this.__getMethod('getTestForLauncher')
                },

                "/tests/:id/tests_groups": {
                    act: this.__getMethod('getTestGroups')
                },
                "/tests_groups/:id/tests" : {
                    act: this.__getMethod('getGroupTests')
                }
            },
            "post": {
                "/tests_groups": {
                    act: this.__getMethod('createGroup')
                },
                "/tests/": {
                    act: this.__getMethod('createTest')
                },
                "/tests/:id/questions": {
                    act: this.__getMethod('createTestQuestion')
                },

                //CUSTOM actions
                "/custom/send_test_to": {
                    act: this.__getMethod('sendTestLinkToEmail')
                },
                "/custom/get_test_link": {
                    act: this.__getMethod('getTestLink')
                },
            },
            "delete":{
                "/tests_groups/:id": {
                    act: this.__getMethod('removeGroup')
                },
                "/tests/:id": {
                    act: this.__getMethod('removeTest')
                },
                "/tests/:id/questions/:qid": {
                    act: this.__getMethod('removeTestQuestion')
                }
            },
            "patch":{
                "/tests_groups/:id": {
                    act: this.__getMethod('updateGroup')
                },
                "/tests/:id": {
                    act: this.__getMethod('updateTest')
                },
                "/tests/:id/questions/:qid": {
                    act: this.__getMethod('updateTestQuestion')
                }
            },
        };
    }
    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getGroups(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await new testService().addUserDataFilter(this.__getUserFilter(req)).getGroupsFilteredCollection(requestObject);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiGroupMapper())
        );
    }

    /**
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async getGroup(req, res)
    {
        let gr = await new testService().addUserDataFilter(this.__getUserFilter(req)).getGroup(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(gr, new ApiGroupMapper())
        );
    }

    /**
     * Create new group of tests and returns created group data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createGroup(req, res)
    {
        let mappedObject = new ApiGroupMapper().toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let gr = await new testService().addUserDataFilter(this.__getUserFilter(req)).createGroup(mappedObject);
        if(!gr)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(gr, new ApiGroupMapper())
        );
    }

    /**
     * Returns updated test group or 404 if group not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async updateGroup(req, res)
    {
        let gr  = await new testService().addUserDataFilter(this.__getUserFilter(req)).getGroup(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Group with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        let mappedObject = new ApiGroupMapper().toServerProperty(req.body);

        let cat = await new testService().updateGroupById(mappedObject, req.params.id);
        if(!cat)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(cat, new ApiGroupMapper())
        );
    }

    /**
     * Remove test group return 201 or 404 if category not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     *
     */
    async removeGroup(req, res)
    {
        let cat = await new testService().addUserDataFilter(this.__getUserFilter(req)).removeById(req.params.id);
        if(!cat)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(201).send();
    }

    /**
     * Returns tests collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getTests(req, res)
    {
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let tests = await new testService().getTestsCollection(requestObject);
        if(!tests)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(tests, ApiTestsMapper)
        );
    }

    /**
     * Returns tests collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getGroupTests(req, res)
    {
        let gr  = await new testService().addUserDataFilter(this.__getUserFilter(req)).getGroup(req.params.id);
        if(!gr)
        {
            return res.status(404).send(
                { message: "Group with id *"+ req.params.id +"* doesn't exists"}
            );
        }
        if(!gr.testsCount)
        {
            return res.status(200).send(
                this.setCollectionResponseData([], ApiTestsMapper)
            );
        }

        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        requestObject.addWhereConditionsForCustomObject('groupsList', gr._id);

        let tests = await new testService().getTestsCollection(requestObject);
        if(!tests)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(tests, ApiTestsMapper)
        );
    }


    /**
     * Returns groups collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getTestGroups(req, res)
    {
        let test = await new testService().addUserDataFilter(this.__getUserFilter(req)).getTest(req.params.id);
        if(!test)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }
        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let cats = await  new testService().getTestGroupsFilteredCollection(requestObject, req.params.id);
        if(!cats)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(cats, new ApiGroupMapper())
        );
    }
    /**
     * Returns test by id or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     *
     */
    async getTestForLauncher(req, res)
    {
        let test = await new testService().getTest(req.params.id);
        if(!test)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }
        let responseData = this.mapEntityToClient(test, ApiTestsMapper);
        responseData.cid = test.cid;

        res.status(200).send(
            responseData
        );
    }

    /**
     * Returns test by id or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     *
     */
    async getTest(req, res)
    {
        let test = await new testService().addUserDataFilter(this.__getUserFilter(req)).getTest(req.params.id);
        if(!test)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(200).send(
            this.mapEntityToClient(test, ApiTestsMapper)
        );
    }

    /**
     * Create new test and returns created test data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createTest(req, res)
    {
        let mappedObject = ApiTestsMapper.toServerProperty(req.body);
        this.__addUidCidToObject(mappedObject, req);
        let test = await new testService().createTest(mappedObject);
        if(!test)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(test, ApiTestsMapper)
        );
    }

    /**
     * Returns updated test or 404 if test not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     *
     */
    async updateTest(req, res)
    {
        let mappedObject = ApiTestsMapper.toServerProperty(req.body);

        let test = await new testService().addUserDataFilter(this.__getUserFilter(req)).updateTest(mappedObject, req.params.id);
        if(!test)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(test, ApiTestsMapper)
        );
    }

    /**
     * Remove test return 201 or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     *
     */
    async removeTest(req, res)
    {
        let test = await new testService().addUserDataFilter(this.__getUserFilter(req)).removeTest(req.params.id);
        if(!test)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(204).send();
    }

    /**
     * Returns test questions collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     */
    async getTestQuestions(req, res)
    {
        let test = await new testService().addUserDataFilter(this.__getUserFilter(req)).getTest(req.params.id);
        if(!test)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.mapCollectionToClient(test.questions || [], ApiTestQuestionMapper)
        );
    }

    /**
     * Create new test question and returns created data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     */
    async createTestQuestion(req, res)
    {
        let mappedObject = ApiTestQuestionMapper.toServerProperty(req.body);

        let testQ = await new testService().createTestQuestion(mappedObject, req.params.id);
        if(!testQ)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(testQ.questions, ApiTestQuestionMapper)
        );
    }

    /**
     * Returns updated question or 404 if test not exist or 400 if there are validation errors
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     * @param {String} req.params.qid  Question id.
     *
     */
    async updateTestQuestion(req, res)
    {
        let mappedObject = ApiTestQuestionMapper.toServerProperty(req.body);

        let testQ = await new testService().updateTestQuestion(mappedObject, req.params.qid, req.params.id);
        if(!testQ)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(testQ, ApiTestQuestionMapper)
        );
    }

    /**
     * Remove question return 201 or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.id  Test id.
     * @param {String} req.params.qid  Question id.
     *
     */
    async removeTestQuestion(req, res)
    {
        let test = await new testService().removeTestQuestion(req.params.qid, req.params.id);
        if(!test)
        {
            return res.status(404).send(
                { message: "Entity not found"}
            );
        }

        res.status(204).send();
    }

    /**
     * Send message template to email return 201 or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.body.tid  Test id.
     * @param {String} req.body.email  user email.
     *
     */
    async sendTestLinkToEmail(req, res)
    {
        const tokenData = await serviceContainer.getService('internal', 'internalCommunicator').createTestLinkData(req.body.tid);
		console.log({links: `${protocol}${launcher_origin}/?token=${tokenData.token}`});
		try {
			const result = await serviceContainer.getService('internal', 'notificationService')
            .sendMailByTpl(
                req.body.email,
                "Your test link",
                path.join(__dirname, '../mailTemplates/inviteToTakeTest'),
                { links: `${protocol}${launcher_origin}/?token=${tokenData.token}` }
            );
        	res.status(204).send({});
		} catch(err) {
			res.status(500).send({
				message: `${err}. Method: sendTestLinkToEmail`
			});
		}
       
    }

    /**
     * Remove question return 201 or 404 if test not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.body.tid  Test id.
     * @param {String} req.body.email  user email.
     *
     */
    async getTestLink(req, res)
    {
        const tokenData = await serviceContainer.getService('internal', 'internalCommunicator').createTestLinkData(req.body.tid);
        res.status(200).send({ links: `${protocol}${launcher_origin}/?token=${tokenData.token}` });
    }
}

module.exports = Api;
