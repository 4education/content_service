//this wrapper should incoupsulate all business logic
const ThemeModel = require("./models/ThemeModel");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
const InternalErrorException = require("./exceptions/InternalError");
const serviceContainer = require("../../libs/ServiceContainer");
const events = require("./config/Events");

class ThemesService
{
    getAll()
    {
        return this.__getModel().getAll();
    }

    getThemesFilteredCollection (requestObject)
    {
        return this.__getModel().executeQuery(requestObject);
    }

    async create(themeData)
    {
        const newTheme = await this.__getModel().create(themeData);
        let createdData = newTheme && newTheme.toObject ? newTheme.toObject() : {};

        this.__getEventManager().dispatch({
            type: events.THEME_CREATED,
            data: createdData
        });
        return createdData;
    }

    async getItem(id)
    {
        const themeData = await this.__getModel().getById(id);
        return themeData && themeData.toObject ? themeData.toObject() : {};

    }

    async updateItem(id, todoItem)
    {
        let item = await this.__getModel().getById(id);
        if(!item)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().update(id, todoItem);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        return updatedItem.toObject() ;
    }

    async deleteItem(id)
    {
        let item = await this.__getModel().getById(id);
        if(!item)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().remove(id);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }
        this.__getEventManager().dispatch({
            type: events.THEME_DELETED,
            data: {_id: id}
        });
        return true ;
    }

    addUserDataFilter(userData)
    {
        this.userTofilter = userData;
        return this;
    }

    __getModel()
    {
        return new ThemeModel().addAdditionalConditions(this.userTofilter);
    }

    __getEventManager()
    {
        return  serviceContainer.getService('internal', 'eventService');
    }
}

module.exports = ThemesService;
