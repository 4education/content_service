const Mapper = require("../../../libs/Mapper");
const ApiTestQuestionMapper = require("./ApiTestQuestionMapper");

class ApiTestMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "name",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "_id",
                toClient: "tid",
                type: "string",
            },
            {
                toServer: "pid",
                toClient: "pid",
                type: "string"
            },
            {
                toServer: "thid",
                toClient: "thid",
                type: "string"
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean"
            },
            {
                toServer: "isTimer",
                toClient: "isTimer",
                type: "boolean",
            },
            {
                toServer: "timer",
                toClient: "timer",
                type: "number",
            },
            {
                toServer: "complexity",
                toClient: "complexity",
                type: "number",
            },
            {
                toServer: "minPassing",
                toClient: "minPassing",
                type: "number",
            },
            {
                toServer: "questions",
                toClient: "questions",
                toServerFunc: (items => {
                    if(items && items.length && items.length > 0)
                    {
                        return items.map( item => {
                            return ApiTestQuestionMapper.toServerProperty(item);
                        })
                    }
                    return items;
                }),
                toClientFunc: (items => {
                    if(items && items.length && items.length > 0)
                    {
                        return items.map( item => {
                            return ApiTestQuestionMapper.toClientProperty(item);
                        })
                    }
                    return items;
                }),
                type: "array",
            },
            {
                toServer: "groupsList",
                toClient: "gid",
                toClientFunc: (items => {
                    if(items && items.length && items.length > 0)
                    {
                        return items.map( item => {
                            if(typeof item === 'object' && item._id)
                            {
                                return item._id
                            }
                            return item;
                        })
                    }
                    return items;
                }),
                //toServerSkipped: true,
                type: "array",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "version",
                toClient: "version",
                toServerSkipped: true,
                type: "string",
            },
        ];
    }
}

    module.exports = new ApiTestMapper();
