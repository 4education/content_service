
const mongooseLib = require("../libs/mongoose");

//run service registration;
require("../RegisterServices");

const InstallFixture = require("./install_fixture.js");

// Configuration
const dbConf = require('../config/db');


mongooseLib.connect(dbConf.db, dbConf.host, dbConf.dbName, dbConf.port);

mongooseLib.setHandler('error', console.error.bind(console, 'connection error:'));

mongooseLib.setHandlerOnce('open', function() {
    console.log("DB connection alive");

    const args = process.argv.slice(2);
    if(!args[0])
    {
        console.log("Script name shpuld be past");
        process.exit(1);
        return;
    }
    switch(args[0])
    {
        case('install_fixture'):
            new InstallFixture().run().then( () => {
                process.exit(1);
            }).catch(err => { console.log(err); process.exit(1);});
        break;
    }
});
