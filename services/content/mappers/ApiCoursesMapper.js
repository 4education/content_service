const Mapper = require("../../../libs/Mapper");
const ValidationError = require("../../../libs/ValidationError")

class ApiCoursesMapper extends Mapper
{
    getPropertyMapping()
    {
        return [

            {
                toServer: "_id",
                toClient: "id",
                type: "string",
                toServerSkipped: true,
            },
            {
                toServer: "name",
                toClient: "name",
                type: "string",
                toServerFunc: (name => {
                    if(!name || !name.length)
                    {
                        throw new ValidationError("Name should be not empty")
                    }
                    return name;
                })
            },
            {
                toServer: "description",
                toClient: "description",
                type: "string",
                toServerFunc: (desc => {
                    if(!desc || !desc.length)
                    {
                        throw new ValidationError("Description should be not empty")
                    }
                    return desc;
                })
            },
            {
                toServer: "titleImageUrl",
                toClient: "titleImageUrl",
                type: "string",
                toServerFunc: (desc => {
                    if(!desc || !desc.length)
                    {
                        throw new ValidationError("titleImageUrl should be not empty")
                    }
                    return desc;
                })
            },
            // {
            //     toServer: "catId",
            //     toClient: "catId",
            //     type: "string",
            //     required: true,
            // },
            {
                toServer: "modulesCount",
                toClient: "modulesCount",
                type: "number"
            },
            // {
            //     toServer: "isActive",
            //     toClient: "isActive",
            //     type: "boolean",
            // },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "statData",
                toClient: "statData",
                toServerSkipped: true,
            },
        ];
    }
}

module.exports = ApiCoursesMapper;
