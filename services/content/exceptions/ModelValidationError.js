const InternalError = require("./InternalError");

class ModelValidationError extends InternalError
{

}

module.exports = ModelValidationError;