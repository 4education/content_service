const Mapper = require("../../../libs/Mapper");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "cid",
                toClient: "cid",
                type: "string",
            },
            {
                toServer: "testsCount",
                toClient: "testsCount",
                type: "boolean"
            },
            {
                toServer: "description",
                toClient: "description",
                type: "string",
            },
            {
                toServer: "price",
                toClient: "price",
                type: "number",
            },
            {
                toServer: "service",
                toClient: "service",
                type: "string",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "lastUpdatedDate",
                toClient: "lastUpdatedDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "additionalInfo",
                toClient: "additionalInfo",
                type: "object",
            },
        ];
    }
}

module.exports = ApiMapper;