const mongoose = require('../../../libs/mongoose').mongoose;
const ModelValidationError = require('../exceptions/ModelValidationError');

// define the testQuestion model schema text, image_text, video_text, test_radio_button, test_checkbox, test_image
const MaterialSectionSchema = new mongoose.Schema({
    type: {type: String, enum: ['text','image_text','video_text','test_radio_button','test_checkbox','test_image']},
    attributes: {type: mongoose.Schema.Types.Mixed, default: {}}
});


class MaterialSectionModel
{

    getSchema()
    {
        return MaterialSectionSchema;
    }

    validateData(question)
    {

    }
}

module.exports = new MaterialSectionModel();