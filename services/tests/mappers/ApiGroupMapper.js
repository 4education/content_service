const Mapper = require("../../../libs/Mapper");
const ValidationError = require("../../../libs/ValidationError")

class ApiGroupMapper extends Mapper
{
    getPropertyMapping()
    {
        return [

            {
                toServer: "_id",
                toClient: "gid",
                type: "string",
                toServerSkipped: true,
            },
            {
                toServer: "name",
                toClient: "name",
                type: "string",
                toServerFunc: (name => {
                    if(!name || !name.length)
                    {
                        throw new ValidationError("Name should be not empty")
                    }
                    return name;
                })
            },
            {
                toServer: "pid",
                toClient: "pid",
                type: "string",
                toServerFunc: (pid => {
                    if(!pid || !pid.length)
                    {
                        throw new ValidationError("pid of publishing should be not empty")
                    }
                    return pid;
                })
            },
            {
                toServer: "thid",
                toClient: "thid",
                type: "string"
            },
            {
                toServer: "testsList",
                toClient: "tids",
                type: "array",
                // toServerFunc: (tests => {
                //     tests.map( test => {
                //         if(!test.tid)
                //         {
                //             throw new ValidationError("Required field *tid* is missing")
                //         }
                //     });
                //     return tests;
                // })

            },
            {
                toServer: "testsCount",
                toClient: "testsCount",
                type: "number"
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            },
        ];
    }
}

module.exports = ApiGroupMapper;
