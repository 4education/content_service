module.exports = {
    secretKey: process.env.SIGNIN_TESTS_TOKEN, //for token generation
    launch_service_url:  'http://'+ process.env.LAUNCH_SERVICE_HOST + ( process.env.LAUNCH_SERVICE_PORT  ? ':'+ process.env.LAUNCH_SERVICE_PORT : ''),
    expiresIn: 86400 // expires in 24 hours
}